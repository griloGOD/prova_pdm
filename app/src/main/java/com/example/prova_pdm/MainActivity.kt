package com.example.prova_pdm

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowDown
import androidx.compose.material.icons.filled.KeyboardArrowUp
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.sp
import androidx.lifecycle.MutableLiveData
import com.example.prova_pdm.ui.theme.Prova_PDMTheme

class MainActivity : ComponentActivity() {

    private val _addCarState = MutableLiveData(AddCarState())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MainContent()
        }
    }
    @Composable
    private fun MainContent(){
        Scaffold(topBar = {
            TopAppBar(
                title = {
                    Text(text = stringResource(id = R.string.app_name))
                },
                backgroundColor = colorResource(id = R.color.purple_500),
                contentColor = Color.White)
        },

            ) {
            FormFields()
        }
    }

    @Composable
    private fun FormFields(){
        val modelState = remember { mutableStateOf("") }
        val priceState = remember { mutableStateOf("") }
        val typeState = remember { mutableStateOf("")}

        var expanded by remember { mutableStateOf(false) }
        val suggestions = listOf("Hatch", "Caminhão", "Motocicleta", "Sedan", "Pickup", "Van", "SUV")
        var selectedText by remember { mutableStateOf("") }

        val icon = if (expanded)
            Icons.Filled.KeyboardArrowUp
        else
            Icons.Filled.KeyboardArrowDown

        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier.fillMaxSize()
        ) {
            OutlinedTextField(
                value = modelState.value,
                onValueChange = {
                    modelState.value = it
                    _addCarState.value = _addCarState.value?.copy(model = it)
                },
                label = { Text(text = stringResource(id = R.string.model_hint))}
            )
            OutlinedTextField(
                value = selectedText,
                onValueChange = {selectedText = it},
                modifier = Modifier.clickable(onClick = { expanded = true }),
                label = { Text(text = stringResource(id = R.string.type_hint))},
                trailingIcon = {
                    Icon(icon,"contentDescription",
                        Modifier.clickable { expanded = !expanded })
                }
            )
            DropdownMenu(
                expanded = expanded,
                onDismissRequest = { expanded = false },
                ) {
                suggestions.forEach { label ->
                    DropdownMenuItem(onClick = {
                        selectedText = label
                        _addCarState.value = _addCarState.value?.copy(type = label)
                    }) {
                        Text(text = label)
                    }
                }
            }

            OutlinedTextField(
                value = priceState.value,
                onValueChange = {
                    priceState.value = it
                    _addCarState.value = _addCarState.value?.copy(price = it.toFloat())
                    },
                label = { Text(text = stringResource(id = R.string.price_hint))},
            )
            Button(onClick = { onAddTapped() }) {
                Text(text = stringResource(id = R.string.add_button_text))
            }
        }
    }

    private fun onAddTapped(){
        val carState = _addCarState.value ?: return
        println(carState)
    }

    @Preview
    @Composable
    private fun DefaultPreview(){
        MainContent()
    }

}