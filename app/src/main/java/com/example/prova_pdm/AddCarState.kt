package com.example.prova_pdm

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class AddCarState(
    val model: String = "",
    val type: String = "",
    val price: Float = 0f
): Parcelable